from flask import Blueprint

base_route = Blueprint('base', __name__)


@base_route.route('/')
def root():
    return 'Добро пожаловать, это домашнее задание №9!'
