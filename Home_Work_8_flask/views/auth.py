from flask import request, render_template, session, url_for, redirect, Blueprint
from sqlalchemy.exc import MultipleResultsFound, NoResultFound
from Home_Work_8_flask.db import db
from Home_Work_8_flask.models import UserModel

auth_route = Blueprint('auth', __name__)


@auth_route.route('/profile')
def profile():
    user = session.get('user')
    return render_template('home_page.html', user=user)


@auth_route.route('/login', methods=('GET', 'POST'))
def login():
    if request.method == 'POST':

        user_name = request.form['username']
        password = request.form['password']

        try:
            user = db.session.query(UserModel.user_name.label('login'),
                                       UserModel.name).filter(
                UserModel.user_name == user_name
            ).filter(
                UserModel.password == password
            ).one()
            '''
            select user.user_name as login, user.name from user where user.user_name = ? and user.password = ?
            '''
        except MultipleResultsFound:
            return render_template('errors/500.html', message='Такого користувача не знайдено')
        except NoResultFound:
            return render_template('errors/500.html', message='Знайдено більше одного користувача.')
        session.clear()
        session['user'] = {'user_name': user.login, 'name': user.name}
        return redirect(url_for('auth.profile'))
    user = session.get('user')
    if user is not None:
        return redirect(url_for('auth.profile'))
    return render_template('register.html')


@auth_route.route('/signup', methods=('GET', 'POST'))
def register():
    if request.method == 'POST':
        user_name = request.form['username']
        password = request.form['password']
        user = UserModel(user_name=user_name, password=password)
        db.session.add(user)
        db.session.commit()
        return redirect(url_for('auth.profile'))
    return render_template('register.html')


@auth_route.route('/logout', methods=('GET', 'POST'))
def logout():
    if 'user' not in session:
        return redirect(url_for('auth.login'))
    session.clear()
    return redirect(url_for('auth.profile'))
