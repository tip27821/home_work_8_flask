import sqlite3 as sq


def get_connection(db_name):
    return sq.connect(db_name)


def init_db(db_name):
    connection = get_connection(db_name)
    cursor = connection.cursor()
    cursor.execute('CREATE TABLE if not exists users('
                   'id integer primary key autoincrement not null,'
                   'user_name text not null,'
                   'password text not null,'
                   'name text);')
    connection.commit()
    connection.close()

